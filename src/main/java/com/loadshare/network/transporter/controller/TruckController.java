package com.loadshare.network.transporter.controller;

import com.loadshare.network.transporter.dao.entity.Truck;
import com.loadshare.network.transporter.dao.TruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/truck")
@SpringBootApplication
@EntityScan( basePackages = {"com.loadshare.network.transporter.dao.entity"} )
@EnableJpaRepositories("com.loadshare.network.transporter.dao")
public class TruckController {

    @Autowired
    private TruckRepository truckRepository;

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Truck> getAllTrucks() {
        return truckRepository.findAll();
    }

    @GetMapping(path = "/search/license-plate")
    public @ResponseBody
    Iterable<Truck> findTruck(@RequestParam("licenseNumber") String licenseNumber) {
        return truckRepository.findTruckByLicense(licenseNumber);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TruckController.class, args);
    }
}
