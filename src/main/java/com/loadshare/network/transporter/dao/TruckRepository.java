package com.loadshare.network.transporter.dao;

import com.loadshare.network.transporter.dao.entity.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TruckRepository extends JpaRepository<Truck,Long> {
    List<Truck> findTruckByLicense(String licenseNumber);

}
