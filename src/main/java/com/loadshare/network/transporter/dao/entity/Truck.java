package com.loadshare.network.transporter.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "truck")
public class Truck {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String license;

    private Long capacity; // capacity in volumetric weight in kg

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "id=" + id +
                ", license='" + license + '\'' +
                ", capacity=" + capacity +
                '}';
    }

}
