CREATE TABLE `truck` (
  `id` bigint(20) AUTO_INCREMENT NOT NULL,
  `capacity` bigint(20) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `truck`
--

INSERT INTO `truck` VALUES (1,100,'KA01 AB 0101');
